# trofeus-api

Projeto criado com finalidade de participação do desafio Ribon para desenvolvedores.

### Instruções de uso

Para executar o projeto, é preciso ter um ambiente configurado com
- [Docker](https://docs.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

#### Comandos iniciais

Após clonar o repositório do projeto, navegue até a pasta do download e copie o arquivo '.env.example' para '.env'

```
~ cd trofeus-app
~ cp .env.example .env
```

Instale as dependências do projeto utilizando a imagem docker do php compose

```
~ docker run --rm -v $(pwd):/app composer install
```

Em seguida, execute o seguinte comando para iniciar a aplicação

```
~ docker-compose up -d
```

Após iniciada, é preciso gerar uma chave ssh, que será armazenada no arquivi '.env'. Para gerar a chave ssh do app, use o seguinte comando

```
~ docker-compose exec trofeus-app php artisan key:generate
```

Feito isso, basta executar as migrations/seeds para inicializar o BD com dados de teste

```
~ docker-compose exec trofeus-app php artisan migrate:fresh --seed
```

Para verificar se a aplicação está respondendo, acesse
[http://localhost/api/monsters]
