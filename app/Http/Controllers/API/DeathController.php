<?php

namespace App\Http\Controllers\API;

use App\Death;
use App\User;
use App\UserDeaths;
use App\UserTrofeus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeathController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Death::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required|integer'
          ]);
        $death = new Death([
            'user_id' => $request->user_id
        ]);
        $death->save();


        // LÓGICA DO TROFÉU
        $usr = User::find($request->user_id);
        $userDeaths;
        $qtdDeaths = 0;

        //verifica se o usuário já possui o valor totalizado
        if($usr->userDeaths == null){
            Log::info('usuário sem deaths TOTAL');
            $qtdDeaths = Death::where(['user_id' => $usr->id])->count();
            $userDeaths = new UserDeaths([
                'user_id' => $usr->id, 
                'value' => $qtdDeaths,
                ]);
        }
        else{
            $userDeaths = UserDeaths::where(['user_id' => $usr->id])->first();
            $userDeaths->value += 1;
        }

        if ($userDeaths->value >= 1 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_TOTAL_DEAHTS_1)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_TOTAL_DEAHTS_1,
                    'name' => 'Troféu DEATH 1',
                    'message' => 'Parabéns, você acumulou 1 DEATH!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userDeaths->value >= 10 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_TOTAL_DEAHTS_2)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_TOTAL_DEAHTS_2,
                    'name' => 'Troféu DEATH 2',
                    'message' => 'Parabéns, você acumulou 10 DEATHS!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userDeaths->value >= 25 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_TOTAL_DEAHTS_3)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_TOTAL_DEAHTS_3,
                    'name' => 'Troféu DEATH 3',
                    'message' => 'Parabéns, você acumulou 25 DEATHS!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userDeaths->value >= 50 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_TOTAL_DEAHTS_4)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_TOTAL_DEAHTS_4,
                    'name' => 'Troféu DEATH 4',
                    'message' => 'Parabéns, você acumulou 50 DEATHS!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userDeaths->value >= 100 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_TOTAL_DEAHTS_5)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_TOTAL_DEAHTS_5,
                    'name' => 'Troféu DEATH 5',
                    'message' => 'Parabéns, você acumulou 100 DEATHS!',
                ]);
    
                $trofeu->save();
            }

        }

        $userDeaths->save();

        return $death;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Death  $death
     * @return \Illuminate\Http\Response
     */
    public function show(Death $death)
    {
        return $death;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Death  $death
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Death $death)
    {
        $request->validate([
            'user_id'=>'required|integer'
          ]);
        $death->user_id = $request->user_id;
        $death->save();
        return $death;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Death  $death
     * @return \Illuminate\Http\Response
     */
    public function destroy(Death $death)
    {
        $death->delete();
        return $death;
    }
}
