<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources([
    'monsters' => 'API\MonsterController',
    'deaths' => 'API\DeathController',
    'collectedCoins' => 'API\CollectedCoinController',
    'killedMonsters' => 'API\KilledMonsterController',
    'userDeaths' => 'API\UserDeathsController',
    'userCollectedCoins' => 'API\UserCollectedCoinController',
    'userKilledMonsters' => 'API\UserKilledMonsterController',
    'userTrofeus' => 'API\UserTrofeusController'
]);