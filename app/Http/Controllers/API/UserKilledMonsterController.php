<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\UserKilledMonster;
use Illuminate\Http\Request;

class UserKilledMonsterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserKilledMonster::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserKilledMonster  $userKilledMonster
     * @return \Illuminate\Http\Response
     */
    public function show(UserKilledMonster $userKilledMonster)
    {
        return $userKilledMonster;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserKilledMonster  $userKilledMonster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserKilledMonster $userKilledMonster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserKilledMonster  $userKilledMonster
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserKilledMonster $userKilledMonster)
    {
        $userKilledMonster->delete();
        return $userKilledMonster;
    }
}
