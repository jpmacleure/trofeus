<?php

use Illuminate\Database\Seeder;

class MonstersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Monster::class, 5)->create()->each(function ($monster) {
            $monster->save();
        });
    }
}
