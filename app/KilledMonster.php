<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KilledMonster extends Model
{
    //Desabilita as colunas created_at e updated_at default da migration
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'monster_id',
    ];

}
