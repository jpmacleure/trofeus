<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrofeus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'user_id', 'message'
    ];

    /*
        CONSTANTES TROFÉUS COLLECTED_COINS
    */
    const TROFEU_COLLECTED_COIN_1 = 101;
    const TROFEU_COLLECTED_COIN_2 = 102;
    const TROFEU_COLLECTED_COIN_3 = 103;
    const TROFEU_COLLECTED_COIN_4 = 104;
    const TROFEU_COLLECTED_COIN_5 = 105;

    /*
        CONSTANTES TROFÉUS TOTAL_DEATHS
    */
    const TROFEU_TOTAL_DEAHTS_1 = 201;
    const TROFEU_TOTAL_DEAHTS_2 = 202;
    const TROFEU_TOTAL_DEAHTS_3 = 203;
    const TROFEU_TOTAL_DEAHTS_4 = 204;
    const TROFEU_TOTAL_DEAHTS_5 = 205;

    /*
        CONSTANTES TROFÉUS KILLED_MONSTERS
        1 User pode ter um nível de troféu, e.g. TROFEU_KILLED_MONSTER_1
        Para cada MONSTER_ID. Dessa forma, o código final do troféu no BD
        é da forma (TROFEU_KILLED_MONSTER_1 + MONSTER_ID)
    */
    const TROFEU_KILLED_MONSTER_1 = 1000;
    const TROFEU_KILLED_MONSTER_2 = 2000;
    const TROFEU_KILLED_MONSTER_3 = 3000;
    const TROFEU_KILLED_MONSTER_4 = 4000;
    const TROFEU_KILLED_MONSTER_5 = 5000;

}
