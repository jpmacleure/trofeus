<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KilledMonster;
use App\User;
use App\Monster;
use App\UserTrofeus;
use App\UserKilledMonster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class KilledMonsterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return KilledMonster::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required|integer',
            'monster_id'=>'required|integer'
          ]);
        $killedMonster = new KilledMonster([
            'user_id' => $request->user_id,
            'monster_id' => $request->monster_id
        ]);
        $killedMonster->save();


        // LÓGICA DO TROFÉU
        $usr = User::find($request->user_id);
        $monster = Monster::find($request->monster_id);
        $userKilledMonster;
        $qtdKilledMonster = 0;

        //verifica se o usuário já possui o valor totalizado
        if(sizeof($usr->userKilledMonsters->where('monster_id', $request->monster_id)) == 0){
            Log::info('usuário sem killed monsters TOTAL');
            $qtdKilledMonster = KilledMonster::where(['user_id' => $usr->id, 'monster_id' => $request->monster_id])->count();
            $userKilledMonster = new UserKilledMonster([
                'user_id' => $usr->id, 
                'monster_id' => $request->monster_id,
                'value' => $qtdKilledMonster,
                ]);
        }
        else{
            Log::info('usuário POSSUI killed monsters TOTAL');
            $userKilledMonster = UserKilledMonster::where(['user_id' => $usr->id, 'monster_id' => $request->monster_id])->first();
            $userKilledMonster->value += 1;
        }

        $userKilledMonster->monster_id = $request->monster_id;

        if ($userKilledMonster->value >= 1 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', (UserTrofeus::TROFEU_KILLED_MONSTER_1+$request->monster_id))) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => (UserTrofeus::TROFEU_KILLED_MONSTER_1+$request->monster_id),
                    'name' => 'Troféu KILLED MONSTERS 1 - ' . $monster->name,
                    'message' => 'Parabéns, você derrotou 1 ' . $monster->name,
                ]);
    
                $trofeu->save();
            }

        }

        if ($userKilledMonster->value >= 100 ) {
            //$userKilledMonster->trofeu_2 = TRUE;


            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', (UserTrofeus::TROFEU_KILLED_MONSTER_2+$request->monster_id))) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => (UserTrofeus::TROFEU_KILLED_MONSTER_2+$request->monster_id),
                    'name' => 'Troféu KILLED MONSTERS 2 - ' . $monster->name,
                    'message' => 'Parabéns, você derrotou 100 ' . $monster->name,
                ]);
    
                $trofeu->save();
            }

        }

        if ($userKilledMonster->value >= 1000 ) {
            //$userKilledMonster->trofeu_3 = TRUE;

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', (UserTrofeus::TROFEU_KILLED_MONSTER_3+$request->monster_id))) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => (UserTrofeus::TROFEU_KILLED_MONSTER_3+$request->monster_id),
                    'name' => 'Troféu KILLED MONSTERS 3 - ' . $monster->name,
                    'message' => 'Parabéns, você derrotou 1000 ' . $monster->name,
                ]);
    
                $trofeu->save();
            }

        }

        if ($userKilledMonster->value >= 10000 ) {
            //$userKilledMonster->trofeu_4 = TRUE;

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', (UserTrofeus::TROFEU_KILLED_MONSTER_4+$request->monster_id))) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => (UserTrofeus::TROFEU_KILLED_MONSTER_4+$request->monster_id),
                    'name' => 'Troféu KILLED MONSTERS 4 - ' . $monster->name,
                    'message' => 'Parabéns, você derrotou 10000 ' . $monster->name,
                ]);
    
                $trofeu->save();
            }

        }

        if ($userKilledMonster->value >= 100000 ) {
            //$useruserKilledMonsterDeaths->trofeu_5 = TRUE;

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', (UserTrofeus::TROFEU_KILLED_MONSTER_5+$request->monster_id))) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => (UserTrofeus::TROFEU_KILLED_MONSTER_5+$request->monster_id),
                    'name' => 'Troféu KILLED MONSTERS 5 - ' . $monster->name,
                    'message' => 'Parabéns, você derrotou 100000 ' . $monster->name,
                ]);
    
                $trofeu->save();
            }
        }

        $userKilledMonster->save();

        return $killedMonster;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KilledMonster  $killedMonster
     * @return \Illuminate\Http\Response
     */
    public function show(KilledMonster $killedMonster)
    {
        return $killedMonster;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KilledMonster  $killedMonster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KilledMonster $killedMonster)
    {
        $request->validate([
            'user_id'=>'required|integer',
            'monster_id'=>'required|integer'
          ]);
        $killedMonster->user_id = $request->user_id;
        $killedMonster->monster_id = $request->monster_id;
        $killedMonster->save();
        return $killedMonster;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KilledMonster  $killedMonster
     * @return \Illuminate\Http\Response
     */
    public function destroy(KilledMonster $killedMonster)
    {
        $killedMonster->delete();
        return $killedMonster;
    }
}
