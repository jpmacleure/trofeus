<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Monster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MonsterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('API /api/monsters');
        return Monster::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
          ]);
        $monster = new Monster([
            'name' => $request->name
        ]);
        $monster->save();
        return $monster;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function show(Monster $monster)
    {
        return $monster;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monster $monster)
    {
        $request->validate([
            'name'=>'required'
          ]);
    
        $monster->name = $request->name;
        $monster->save();

        return $monster;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function destroy(Monster $monster)
    {
        $monster->delete();
        return $monster;
    }
}
