<?php

namespace App\Http\Controllers\API;

use App\CollectedCoin;
use App\UserCollectedCoin;
use App\User;
use App\UserTrofeus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CollectedCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CollectedCoin::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required|integer',
            'value'=>'required|integer'
          ]);
        $collectedCoin = new CollectedCoin([
            'user_id' => $request->user_id,
            'value' => $request->value
        ]);
        $collectedCoin->save();


        // LÓGICA DO TROFÉU
        $usr = User::find($request->user_id);
        $userCollectedCoin;
        $qtdCoins = 0;

        //verifica se o usuário já possui o valor totalizado
        if($usr->userCollectedCoins == null){
            Log::info('usuário sem coins TOTAL');
            $qtdCoins = CollectedCoin::where(['user_id' => $usr->id])->sum('value');
            $userCollectedCoin = new UserCollectedCoin([
                'user_id' => $usr->id, 
                'value' => $qtdCoins,
                ]);
        }
        else{
            $userCollectedCoin = UserCollectedCoin::where(['user_id' => $usr->id])->first();
            $userCollectedCoin->value += $request->value;
        }

        if ($userCollectedCoin->value >= 1 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_COLLECTED_COIN_1)) == 0 ){

                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_COLLECTED_COIN_1,
                    'name' => 'Troféu COLLECTED COINS 1',
                    'message' => 'Parabéns, você coletou 1 moeda!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userCollectedCoin->value >= 100 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_COLLECTED_COIN_2)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_COLLECTED_COIN_2,
                    'name' => 'Troféu COLLECTED COINS 2',
                    'message' => 'Parabéns, você coletou 100 moeda!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userCollectedCoin->value >= 1000 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_COLLECTED_COIN_3)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_COLLECTED_COIN_3,
                    'name' => 'Troféu COLLECTED COINS 3',
                    'message' => 'Parabéns, você coletou 1000 moeda!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userCollectedCoin->value >= 10000 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_COLLECTED_COIN_4)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_COLLECTED_COIN_4,
                    'name' => 'Troféu COLLECTED COINS 4',
                    'message' => 'Parabéns, você coletou 10000 moeda!',
                ]);
    
                $trofeu->save();
            }

        }

        if ($userCollectedCoin->value >= 100000 ) {

            //Caso não possua o troféu
            if ( sizeof($usr->trofeus->where('code', UserTrofeus::TROFEU_COLLECTED_COIN_5)) == 0 ){
                
                $trofeu = new UserTrofeus([
                    'user_id' => $usr->id,
                    'code' => UserTrofeus::TROFEU_COLLECTED_COIN_5,
                    'name' => 'Troféu COLLECTED COINS 5',
                    'message' => 'Parabéns, você coletou 100000 moeda!',
                ]);
    
                $trofeu->save();
            }

        }

        $userCollectedCoin->save();


        return $collectedCoin;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CollectedCoin  $collectedCoin
     * @return \Illuminate\Http\Response
     */
    public function show(CollectedCoin $collectedCoin)
    {
        return $collectedCoin;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CollectedCoin  $collectedCoin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CollectedCoin $collectedCoin)
    {
        $request->validate([
            'user_id'=>'required|integer',
            'value'=>'required|integer'
        ]);
        
        $collectedCoin->user_id = $request->user_id;
        $collectedCoin->value = $request->value;
        $collectedCoin->save();
        return $collectedCoin;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CollectedCoin  $collectedCoin
     * @return \Illuminate\Http\Response
     */
    public function destroy(CollectedCoin $collectedCoin)
    {
        $collectedCoin->delete();
        return $collectedCoin;
    }
}
