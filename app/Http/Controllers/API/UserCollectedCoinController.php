<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\UserCollectedCoin;
use Illuminate\Http\Request;

class UserCollectedCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserCollectedCoin::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserCollectedCoin  $userCollectedCoin
     * @return \Illuminate\Http\Response
     */
    public function show(UserCollectedCoin $userCollectedCoin)
    {
        return $userCollectedCoin;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserCollectedCoin  $userCollectedCoin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCollectedCoin $userCollectedCoin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCollectedCoin  $userCollectedCoin
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCollectedCoin $userCollectedCoin)
    {
        //
    }
}
