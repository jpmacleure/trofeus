<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CollectedCoin;
use App\User;
use Faker\Generator as Faker;

$factory->define(CollectedCoin::class, function (Faker $faker) {
    return [
        'user_id' => User::find(rand(1, 5))->id,
        'value' => rand(1, 100),
    ];
});
