<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Death;
use App\User;
use Faker\Generator as Faker;

$factory->define(Death::class, function (Faker $faker) {
    return [
        'user_id' => User::find(rand(1, 5))->id,
    ];
});
