<?php

namespace App\Http\Controllers\API;

use App\UserTrofeus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserTrofeusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserTrofeus::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'=>'required|integer',
            'user_id'=>'required|integer',
            'name'=>'required|string'
          ]);
        $userTrofeu = new UserTrofeus([
            'code' => $request->code,
            'user_id' => $request->code,
            'name' => $request->name,
            'message' => $request->message
        ]);

        $userTrofeu->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserTrofeus  $userTrofeus
     * @return \Illuminate\Http\Response
     */
    public function show(UserTrofeus $userTrofeus)
    {
        return $userTrofeus;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserTrofeus  $userTrofeus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTrofeus $userTrofeus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserTrofeus  $userTrofeus
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTrofeus $userTrofeus)
    {
        //
    }
}
