<?php

use Illuminate\Database\Seeder;

class CollectedCoinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CollectedCoin::class, 20)->create()->each(function ($collectedCoin) {
            $collectedCoin->save();
        });
    }
}
