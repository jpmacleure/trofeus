<?php

use Illuminate\Database\Seeder;

class DeathsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Death::class, 23)->create()->each(function ($death) {
            $death->save();
        });
    }
}
