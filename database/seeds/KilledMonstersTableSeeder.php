<?php

use Illuminate\Database\Seeder;

class KilledMonstersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\KilledMonster::class, 20)->create()->each(function ($killedMonster) {
            $killedMonster->save();
        });
    }
}
