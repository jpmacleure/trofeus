<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\UserDeaths;
use Illuminate\Http\Request;

class UserDeathsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserDeaths::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserDeaths  $userDeaths
     * @return \Illuminate\Http\Response
     */
    public function show(UserDeaths $userDeaths)
    {
        return $userDeaths;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserDeaths  $userDeaths
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDeaths $userDeaths)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDeaths  $userDeaths
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDeaths $userDeaths)
    {
        //
    }
}
