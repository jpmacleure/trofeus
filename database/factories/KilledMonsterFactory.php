<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\KilledMonster;
use App\User;
use App\Monster;
use Faker\Generator as Faker;

$factory->define(KilledMonster::class, function (Faker $faker) {
    return [
        'user_id' => User::find(rand(1, 5))->id,
        'monster_id' => Monster::find(rand(1, 5))->id,
    ];
});
