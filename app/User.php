<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the deats for the user.
     */
    public function deaths()
    {
        return $this->hasMany('App\Death');
    }

    /**
     * Get the collected coins for the user.
     */
    public function collectedCoins()
    {
        return $this->hasMany('App\CollectedCoin');
    }

    /**
     * Get the killed monsters for the user.
     */
    public function killedMonsters()
    {
        return $this->hasMany('App\KilledMonster');
    }

    /**
     * Get the user deaths TOTAL.
     */
    public function userDeaths()
    {
        return $this->hasOne('App\UserDeaths');
    }

     /**
     * Get the user collected coins TOTAL.
     */
    public function userCollectedCoins()
    {
        return $this->hasOne('App\UserCollectedCoin');
    }

     /**
     * Get the user killed monsters TOTAL/monster.
     */
    public function userKilledMonsters()
    {
        return $this->hasMany('App\UserKilledMonster');
    }

    /**
     * Get the trofeus for the user.
     */
    public function trofeus()
    {
        return $this->hasMany('App\UserTrofeus');
    }
}
